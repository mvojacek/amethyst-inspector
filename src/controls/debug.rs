use crate::prelude::*;
use std::fmt::Debug;
use std::collections::HashMap;
use std::hash::Hash;
use amethyst::core::Parent;

#[macro_export]
macro_rules! debug_inspect_control {
    ($($typ:ident$(<$($gen:ident),+>)*),+) => {
        $(
            $crate::__impl_debug_inspect_control_generic_once!($typ ,, $($($gen),+)*);
        )+
    }
}

#[macro_export]
#[doc(hidden)]
macro_rules! __impl_debug_inspect_control_generic_once {
    ($typ:ident) => { $crate::__impl_debug_inspect_control_generic_once!($typ ,,); };
	($typ:ident ,, $($($gen:ident),+)*) => {
        impl<'control, 'resource: 'control, $($($gen: ::std::fmt::Debug),+)*> InspectControl<'control, 'resource> for &'control mut $typ$(<$($gen),+>)* {
            type SystemData = ();
            type Builder = $crate::DebugInspectControlBuilder<'control, $typ$(<$($gen),+>)*>;
        }
	}
}

pub struct DebugInspectControlBuilder<'control, T: Debug> {
    pub value: &'control mut T,
    pub label: Option<&'control imgui::ImStr>,
}

impl<'control, 'resource: 'control, T: Debug> InspectControlBuilder<'control, 'resource, &'control mut T> for DebugInspectControlBuilder<'control, T>
    where &'control mut T: InspectControl<'control, 'resource> {
    fn new(value: &'control mut T) -> Self {
        Self { value, label: None }
    }
    fn label(mut self, label: &'control imgui::ImStr) -> Self {
        self.label = Some(label);
        self
    }
    fn build(self) {
        amethyst_imgui::with(|ui| {
            use amethyst_imgui::imgui::ImString;
            let text: ImString = format!("{:?}", self.value).into();
            ui.label_text(self.label.unwrap_or_default(), &text);
        });
    }
}

debug_inspect_control![
    Vec<T>, Option<T>, Parent
];

impl<'control, 'resource: 'control, K: Debug + Hash + Eq, V: Debug> InspectControl<'control, 'resource> for &'control mut HashMap<K, V> {
    type SystemData = ();
    type Builder = DebugInspectControlBuilder<'control, HashMap<K, V>>;
}